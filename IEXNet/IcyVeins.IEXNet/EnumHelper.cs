﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace IcyVeins.IEXNet
{
    internal static class EnumHelper
    {
        private static Dictionary<Type, Dictionary<Enum, string>> _cache = new Dictionary<Type, Dictionary<Enum, string>>();

        public static string GetParameter(this Enum @enum)
        {
            var type = @enum.GetType();
            if (_cache.TryGetValue(type, out var valueMapping))
                return valueMapping[@enum];

            valueMapping = new Dictionary<Enum, string>();
            _cache.Add(type, valueMapping);

            var allValues = Enum.GetValues(type).Cast<Enum>();
            var members = type.GetMembers();
            var mapped = allValues.Select(v => new { Enum = v, Member = members.FirstOrDefault(m => m.Name == v.ToString()) })
                .Where(o => o.Member != null)
                .ToArray();

            foreach (var mappedEnum in mapped)
            {
                var attribute = mappedEnum.Member.GetCustomAttribute<EnumParameter>();
                var value = !string.IsNullOrWhiteSpace(attribute?.Value) ? attribute?.Value : mappedEnum.Enum.ToString();
                valueMapping.Add(mappedEnum.Enum, value);
            }

            return valueMapping[@enum];
        }
    }
}
