﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace IcyVeins.IEXNet
{
    internal static class JTokenHelper
    {
        public static JToken FindProperty(this JToken token, string key)
        {
            if (!(token is JObject jObject))
                return null;

            return jObject.Properties().FirstOrDefault(p => EqualsIgnoreCase(p.Name, key))?.Value;
        }

        private static bool EqualsIgnoreCase(string s1, string s2)
        {
            return string.Equals(s1, s2, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
