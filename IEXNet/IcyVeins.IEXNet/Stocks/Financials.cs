﻿namespace IcyVeins.IEXNet.Stocks
{
    public class FinancialsData
    {
        public string Symbol { get; set; }
        public FinancialsDataPoint[] Financials { get; set; }
    }

    public class FinancialsDataPoint
    {
        public string ReportDate { get; set; }
        public double? GrossProfit { get; set; }
        public double? CostOfRevenue { get; set; }
        public double? OperatingRevenue { get; set; }
        public double? TotalRevenue { get; set; }
        public double? OperatingIncome { get; set; }
        public double? NetIncome { get; set; }
        public double? ResearchAndDevelopment { get; set; }
        public double? OperatingExpense { get; set; }
        public double? CurrentAssets { get; set; }
        public double? TotalAssets { get; set; }
        public double? TotalLiabilities { get; set; }
        public double? CurrentCash { get; set; }
        public double? CurrentDebt { get; set; }
        public double? TotalCash { get; set; }
        public double? TotalDebt { get; set; }
        public double? ShareholderEquity { get; set; }
        public double? CashChange { get; set; }
        public double? CashFlow { get; set; }
        public string OperatingGainsLosses { get; set; }
    }
}
