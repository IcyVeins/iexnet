﻿using System.Collections.Generic;

namespace IcyVeins.IEXNet.Stocks
{
    public class LargestTrades : List<LargestTradesData>
    {
    }

    public class LargestTradesData
    {
        /// <summary>
        /// Refers to the price of the trade.
        /// </summary>
        public double? Price { get; set; }

        /// <summary>
        /// Refers to the number of shares of the trade.
        /// </summary>
        public double? Size { get; set; }

        /// <summary>
        /// Refers to the time of the trade.
        /// </summary>
        public double? Time { get; set; }

        /// <summary>
        /// Formatted time string as HH:MM:SS
        /// </summary>
        public string TimeLabel { get; set; }

        /// <summary>
        /// Refers to the venue where the trade occurred.None refers to a TRF(off exchange) trade.
        /// </summary>
        public string Venue { get; set; }

        /// <summary>
        /// Formatted venue name where the trade occurred.
        /// </summary>
        public string VenueName { get; set; }
    }
}
