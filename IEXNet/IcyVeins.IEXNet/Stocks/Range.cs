﻿using IcyVeins.IEXNet.Stocks.Decorators;
using System.Reflection;

namespace IcyVeins.IEXNet.Stocks
{
    public enum Range
    {
        /// <summary>
        /// One month (default)	Historically adjusted market-wide data
        /// </summary>
        [EnumParameter("1m")] OneMonth,

        /// <summary>
        /// Three months historically adjusted market-wide data
        /// </summary>
        [EnumParameter("3m")] ThreeMonths,

        /// <summary>
        /// Six months historically adjusted market-wide data
        /// </summary>
        [EnumParameter("6m")] SixMonths,

        /// <summary>
        /// Year-to-date historically adjusted market-wide data
        /// </summary>
        [EnumParameter("ytd")] YearToDate,

        /// <summary>
        /// One year historically adjusted market-wide data
        /// </summary>
        [EnumParameter("1y")] OneYear,

        /// <summary>
        /// Two years historically adjusted market-wide data
        /// </summary>
        [EnumParameter("2y")] TwoYears,

        /// <summary>
        /// Five years historically adjusted market-wide data
        /// </summary>
        [EnumParameter("5y")] FiveYears,

        /// <summary>
        /// One day	IEX-only data by minute.
        /// Only supported on Chart data
        /// </summary>
        [EnumParameter("1d")] OneDay,
    }
}
