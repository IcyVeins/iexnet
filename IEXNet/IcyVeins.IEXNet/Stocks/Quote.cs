﻿namespace IcyVeins.IEXNet.Stocks
{
    public class Quote
    {
        /// <summary>
        /// Refers to the stock ticker.
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// Refers to the company name.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// refers to the primary listings exchange.
        /// </summary>        
        public string PrimaryExchange { get; set; }

        /// <summary>
        /// refers to the sector of the stock.
        /// </summary>
        public string Sector { get; set; }

        /// <summary>
        /// refers to the source of the latest price. ("tops", "sip", "previousclose" or "close")
        /// </summary>
        public string CalculationPrice { get; set; }

        /// <summary>
        /// Refers to the official open price.
        /// </summary>
        public double? Open { get; set; }

        /// <summary>
        /// Refers to the official listing exchange time for the open.
        /// </summary>
        public double? OpenTime { get; set; }

        /// <summary>
        /// Refers to the official close price.
        /// </summary>
        public double? Close { get; set; }

        /// <summary>
        /// Refers to the official listing exchange time for the close.
        /// </summary>
        public double? CloseTime { get; set; }

        /// <summary>
        /// Refers to the market-wide highest price from the SIP. 15 minute delayed
        /// </summary>
        public double? High { get; set; }

        /// <summary>
        /// Refers to the market-wide lowest price from the SIP. 15 minute delayed
        /// </summary>
        public double? Low { get; set; }

        /// <summary>
        /// Refers to the latest price being the IEX real time price, the 15 minute delayed market price, or the previous close price.
        /// </summary>
        public double? LatestPrice { get; set; }

        /// <summary>
        /// Refers to the source of latestPrice. ("IEX real time price", "15 minute delayed price", "Close" or "Previous close")
        /// </summary>
        public string LatestSource { get; set; }

        /// <summary>
        /// Refers to a human readable time of the latestPrice.The format will vary based on latestSource.
        /// </summary>
        public string LatestTime { get; set; }

        /// <summary>
        /// Refers to the update time of latestPrice in milliseconds since midnight Jan 1, 1970.
        /// </summary>
        public double? LatestUpdate { get; set; }

        /// <summary>
        /// Refers to the total market volume of the stock.
        /// </summary>
        public double? LatestVolume { get; set; }

        /// <summary>
        /// Refers to last sale price of the stock on IEX. (Refer to the attribution section above.)
        /// </summary>
        public double? IexRealtimePrice { get; set; }

        /// <summary>
        /// Refers to last sale size of the stock on IEX.
        /// </summary>
        public double? IexRealtimeSize { get; set; }

        /// <summary>
        /// refers to the last update time of the data in milliseconds since midnight Jan 1, 1970 UTC or -1 or 0. If the value is -1 or 0, IEX has not quoted the symbol in the trading day.
        /// </summary>
        public double? IexLastUpdated { get; set; }

        /// <summary>
        /// Refers to the 15 minute delayed market price during normal market hours 9:30 - 16:00.
        /// </summary>
        public double? DelayedPrice { get; set; }

        /// <summary>
        /// Refers to the time of the delayed market price during normal market hours 9:30 - 16:00.
        /// </summary>
        public double? DelayedPriceTime { get; set; }

        /// <summary>
        /// Refers to the 15 minute delayed market price outside normal market hours 8:00 - 9:30 and 16:00 - 17:00.
        /// </summary>
        public double? ExtendedPrice { get; set; }

        /// <summary>
        /// Is calculated using extendedPrice from calculationPrice.
        /// </summary>
        public double? ExtendedChange { get; set; }
        /// <summary>
        /// Is calculated using extendedPrice from calculationPrice.
        /// </summary>
        public double? ExtendedChangePercent { get; set; }

        /// <summary>
        /// Refers to the time of the delayed market price outside normal market hours 8:00 - 9:30 and 16:00 - 17:00.
        /// </summary>
        public double? ExtendedPriceTime { get; set; }

        /// <summary>
        /// Is calculated using calculationPrice from previousClose.
        /// </summary>
        public double? Change { get; set; }

        /// <summary>
        /// Is calculated using calculationPrice from previousClose.
        /// </summary>
        public double? ChangePercent { get; set; }

        /// <summary>
        /// Refers to IEX’s percentage of the market in the stock.
        /// </summary>
        public double? IexMarketPercent { get; set; }

        /// <summary>
        /// Refers to shares traded in the stock on IEX.
        /// </summary>
        public double? IexVolume { get; set; }

        /// <summary>
        /// Refers to the 30 day average volume on all markets.
        /// </summary>
        public double? AvgTotalVolume { get; set; }

        /// <summary>
        /// Refers to the best bid price on IEX.
        /// </summary>
        public double? IexBidPrice { get; set; }

        /// <summary>
        /// Refers to amount of shares on the bid on IEX.
        /// </summary>
        public double? IexBidSize { get; set; }

        /// <summary>
        /// refers to the best ask price on IEX.
        /// </summary>
        public double? IexAskPrice { get; set; }

        /// <summary>
        /// Refers to amount of shares on the ask on IEX.
        /// </summary>
        public double? IexAskSize { get; set; }

        /// <summary>
        /// Is calculated in real time using calculationPrice.
        /// </summary>
        public double? MarketCap { get; set; }

        /// <summary>
        /// Iis calculated in real time using calculationPrice.
        /// </summary>
        public double? PeRatio { get; set; }

        /// <summary>
        /// Refers to the adjusted 52 week high.
        /// </summary>
        public double? Week52High { get; set; }

        /// <summary>
        /// Refers to the adjusted 52 week low.
        /// </summary>
        public double? Week52Low { get; set; }

        /// <summary>
        /// refers to the price change percentage from start of year to previous close.
        /// </summary>
        public double? YtdChange { get; set; }
    }
}
