﻿namespace IcyVeins.IEXNet.Stocks
{
    public class DelayedQuote
    {
        /// <summary>
        /// Refers to the stock ticker.
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// Refers to the 15 minute delayed market price.
        /// </summary>
        public double? DelayedPrice { get; set; }

        /// <summary>
        /// Refers to the 15 minute delayed last trade size.
        /// </summary>
        public double? DelayedSize { get; set; }

        /// <summary>
        /// Refers to the time of the delayed market price.
        /// </summary>
        public double? DelayedPriceTime { get; set; }

        /// <summary>
        /// Refers to when IEX processed the SIP price.
        /// </summary>
        public double? ProcessedTime { get; set; }
    }
}
