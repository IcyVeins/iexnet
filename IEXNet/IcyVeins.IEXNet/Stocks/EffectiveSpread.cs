﻿using System.Collections.Generic;

namespace IcyVeins.IEXNet.Stocks
{
    public class EffectiveSpread : List<EffectiveSpreadData>
    {
    }

    public class EffectiveSpreadData
    {
        /// <summary>
        /// Refers to the eligible shares used for calculating effectiveSpread and priceImprovement
        /// </summary>
        public string Volume { get; set; }

        /// <summary>
        /// Refers to the Market Identifier Code(MIC)
        /// </summary>
        public string Venue { get; set; }

        /// <summary>
        /// Refers to a readable version of the venue defined by IEX
        /// </summary>
        public string VenueName { get; set; }

        /// <summary>
        /// Is designed to measure marketable orders executed in relation to the market center’s quoted spread and takes into account hidden and midpoint liquidity available at each market center in dollars
        /// </summary>
        public double? EffectiveSpread { get; set; }

        /// <summary>
        /// A ratio calculated by dividing a market center’s effective spread by the NBBO quoted spread
        /// </summary>
        public double? EffectiveQuoted { get; set; }

        /// <summary>
        /// The average amount of price improvement in dollars per eligible share executed
        /// </summary>
        public double? PriceImprovement { get; set; }
    }
}
