﻿using IcyVeins.IEXNet.Stocks.Decorators;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace IcyVeins.IEXNet.Stocks
{
    public class StockQueryResponse
    {
        public ReadOnlyCollection<string> Symbols => _symbols.AsReadOnly();

        private readonly List<string> _symbols;
        private readonly Dictionary<Type, EndPointDecorator> _decoratorMap;
        private readonly JToken _content;

        internal StockQueryResponse() { }
        internal StockQueryResponse(List<string> symbols, List<EndPointDecorator> decorators, string content)
        {
            _symbols = symbols;
            _decoratorMap = decorators?.ToDictionary(d => d.GetDataType());
            _content = !string.IsNullOrWhiteSpace(content) ? JToken.Parse(content) : null;
        }

        public Chart GetChart(string symbol)
        {
            return Get<Chart>(symbol);
        }

        public Company GetCompany(string symbol)
        {
            return Get<Company>(symbol);
        }

        public DelayedQuote GetDelayedQuote(string symbol)
        {
            return Get<DelayedQuote>(symbol);
        }

        public Dividends GetDividends(string symbol)
        {
            return Get<Dividends>(symbol);
        }

        public Earnings GetEarnings(string symbol)
        {
            return Get<Earnings>(symbol);
        }

        public EffectiveSpread GetEffectiveSpread(string symbol)
        {
            return Get<EffectiveSpread>(symbol);
        }

        public FinancialsData GetFinancials(string symbol)
        {
            return Get<FinancialsData>(symbol);
        }

        public KeyStats GetKeyStats(string symbol)
        {
            return Get<KeyStats>(symbol);
        }

        public LargestTrades GetLargestTrades(string symbol)
        {
            return Get<LargestTrades>(symbol);
        }

        public Peers GetPeers(string symbol)
        {
            return Get<Peers>(symbol);
        }

        public Quote GetQuote(string symbol)
        {
            return Get<Quote>(symbol);
        }

        public T Get<T>(string symbol)
        {
            if (_content == null || string.IsNullOrWhiteSpace(symbol) || !ContainsSymbol(symbol))
                return default(T);

            if (_decoratorMap == null || !_decoratorMap.TryGetValue(typeof(T), out var decorator))
                return default(T);

            if (_symbols.Count == 1 && _decoratorMap.Count == 1)
                return (T)decorator.Deserialize(_content);

            if (_symbols.Count == 1)
                return FromSingleSymbolQuery<T>(decorator);

            return GetFromMarketRequest<T>(symbol, decorator);
        }

        private bool ContainsSymbol(string symbol)
        {
            return _symbols != null && _symbols.Contains(symbol, StringComparer.InvariantCultureIgnoreCase);
        }

        private T FromSingleSymbolQuery<T>(EndPointDecorator decorator)
        {
            return GetFromToken<T>(_content, decorator);
        }

        private T GetFromMarketRequest<T>(string symbol, EndPointDecorator decorator)
        {
            var property = _content.FindProperty(symbol);
            return property != null ? GetFromToken<T>(property, decorator) : default(T);
        }

        private static T FromSymbolData<T>(JToken token, EndPointDecorator decorator)
        {
            var endpoint = decorator.EndPoint;
            var value = token.FindProperty(endpoint);
            return value != null ? (T)decorator.Deserialize(value) : default(T);
        }

        private static T GetFromToken<T>(JToken token, EndPointDecorator decorator)
        {
            var endpoint = decorator.EndPoint;
            var data = token.FindProperty(endpoint);
            return data != null ? (T)decorator.Deserialize(data) : default(T);
        }

        private static bool EqualsIgnoreCase(string s1, string s2)
        {
            return string.Equals(s1, s2, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
