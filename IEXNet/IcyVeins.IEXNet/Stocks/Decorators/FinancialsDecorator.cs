﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class FinancialsDecorator : EndPointDecorator<FinancialsData>
    {
        internal override string EndPoint => "financials";
    }
}
