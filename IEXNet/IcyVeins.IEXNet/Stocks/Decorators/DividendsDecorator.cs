﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class DividendsDecorator : EndPointDecorator<Dividends>
    {
        internal override string EndPoint => "dividends";

        [Parameter] internal Range? Range { get; set; }

        public DividendsDecorator(Range? range)
        {
            Range = range;
        }

        internal override string GetDirectDecoration()
        {
            var d = EndPoint;
            if (Range.HasValue)
                d = $"{EndPoint}/{Range.Value.GetParameter()}";

            return d;
        }
    }
}
