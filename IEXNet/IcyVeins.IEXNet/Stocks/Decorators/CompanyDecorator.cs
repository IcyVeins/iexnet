﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal sealed class CompanyDecorator : EndPointDecorator<Company>
    {
        internal override string EndPoint => "company";
    }
}
