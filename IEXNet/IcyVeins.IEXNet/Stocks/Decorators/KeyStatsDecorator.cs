﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class KeyStatsDecorator : EndPointDecorator<KeyStats>
    {
        internal override string EndPoint => "stats";
    }
}
