﻿using System;
using Newtonsoft.Json.Linq;

namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class ChartDecorator : EndPointDecorator<Chart>
    {
        internal override string EndPoint => "chart";

        [Parameter] internal Range? Range { get; set; }
        internal DateTime? Date { get; set; }
        internal bool? Dynamic { get; set; }

        /// <summary>
        /// If true, 1d chart will reset at midnight instead of the default behavior of 9:30am ET.
        /// </summary>
        [Parameter] internal bool? ChartReset { get; set; }

        /// <summary>
        /// If true, runs a polyline simplification using the Douglas-Peucker algorithm. This is useful if plotting sparkline charts.
        /// </summary>
        [Parameter] internal bool? ChartSimplify { get; set; }

        /// <summary>
        /// If passed, chart data will return every Nth element as defined by chartInterval
        /// </summary>
        [Parameter] internal int? ChartInterval { get; set; }

        /// <summary>
        /// If true, changeOverTime and marketChangeOverTime will be relative to previous day close instead of the first value.
        /// </summary>
        [Parameter] internal bool? ChangeFromClose { get; set; }

        /// <summary>
        /// If passed, chart data will return the last N elements
        /// </summary>
        [Parameter] internal int? ChartLast { get; set; }

        internal ChartDecorator(Range range, bool? chartReset, bool? chartSimplify, bool? chartInterval, bool? changeFromClose, int? chartLast)
            : this(chartReset, chartSimplify,chartInterval, changeFromClose, chartLast)
        {
            Range = range;
        }

        internal ChartDecorator(DateTime date, bool? chartReset, bool? chartSimplify, bool? chartInterval, bool? changeFromClose, int? chartLast)
            : this(chartReset, chartSimplify, chartInterval, changeFromClose, chartLast)
        {
            Date = date;
        }

        internal ChartDecorator(bool dynamic, bool? chartReset, bool? chartSimplify, bool? chartInterval, bool? changeFromClose, int? chartLast)
            : this(chartReset, chartSimplify, chartInterval, changeFromClose, chartLast)
        {
            Dynamic = dynamic;
        }

        internal ChartDecorator(bool? chartReset, bool? chartSimplify, bool? chartInterval, bool? changeFromClose, int? chartLast)
        {
            ChartReset = chartReset;
            ChartSimplify = chartSimplify;
            ChartInterval = ChartInterval;
            ChangeFromClose = changeFromClose;
            ChartLast = chartLast;
        }
        
        internal override string GetDirectDecoration()
        {
            var d = EndPoint;
            if (Range.HasValue)
                d = $"{EndPoint}/{Range.Value.GetParameter()}";
            else if (Date.HasValue)
                d = $"{EndPoint}/date/{Date:yyyyMMdd}";
            else if (Dynamic.HasValue && Dynamic.Value)
                d = $"{EndPoint}/dynamic";

            var @params = GetConcatenatedParameters();
            return d + @params;
        }

        protected override Chart DeserializeData(JToken content)
        {
            if (!Dynamic.HasValue || !Dynamic.Value)
                return base.DeserializeData(content);

            // Dynamic response: { range: "x", data: [...} }
            var token = content.SelectToken("data");
            return token.ToObject<Chart>();
        }
    }
}
