﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class PeersDecorator : EndPointDecorator<Peers>
    {
        internal override string EndPoint => "peers";
    }
}
