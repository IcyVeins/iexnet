﻿using System;

namespace IcyVeins.IEXNet.Stocks.Decorators
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false)]
    internal sealed class ParameterAttribute : Attribute
    {
        internal readonly string Name;

        public ParameterAttribute(string name = null)
        {
            Name = name;
        }
    }
}
