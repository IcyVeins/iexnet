﻿using System;

namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class EffectiveSpreadDecorator : EndPointDecorator<EffectiveSpread>
    {
        internal override string EndPoint => "effective-spread";
    }
}
