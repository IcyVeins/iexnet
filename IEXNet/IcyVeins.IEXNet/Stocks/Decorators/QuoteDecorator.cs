﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class QuoteDecorator : EndPointDecorator<Quote>
    {
        internal override string EndPoint => "quote";

        /// <summary>
        /// If set to true, all percentage values will be multiplied by a factor of 100
        /// </summary>
        [Parameter] internal bool? DisplayPercent { get; set; }

        internal QuoteDecorator(bool? displayPercent)
        {
            DisplayPercent = displayPercent;
        }
    }
}
