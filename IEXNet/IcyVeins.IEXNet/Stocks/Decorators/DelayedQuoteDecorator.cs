﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal sealed class DelayedQuoteDecorator : EndPointDecorator<DelayedQuote>
    {
        internal override string EndPoint => "delayed-quote";
    }
}
