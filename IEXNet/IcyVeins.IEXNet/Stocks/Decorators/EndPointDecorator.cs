﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal abstract class EndPointDecorator
    {
        internal abstract string EndPoint { get; }
        internal abstract object Deserialize(JToken content);
        internal abstract Type GetDataType();
        internal abstract string GetDirectDecoration();
        internal abstract Dictionary<string, string> GetParameters();
    }

    internal abstract class EndPointDecorator<T> : EndPointDecorator
    {
        internal override sealed object Deserialize(JToken content)
        {
            return DeserializeData(content);
        }

        internal sealed override Type GetDataType()
        {
            return typeof(T);
        }

        internal override string GetDirectDecoration()
        {
            var @params = GetConcatenatedParameters();
            return $"{EndPoint}{@params}";
        }

        internal sealed override Dictionary<string, string> GetParameters()
        {
            var properties = GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.GetProperty);
            var @params = properties.Select(pi => new { Info = pi, ParamAttr = pi.GetCustomAttribute<ParameterAttribute>() })
                .Where(io => io.ParamAttr != null);
            var values = @params.Select(io => new
            {
                Name = !string.IsNullOrWhiteSpace(io.ParamAttr.Name) ? io.ParamAttr.Name : io.Info.Name,
                Value = GetParameterValue(io.Info)
            })
            .Where(v => v.Value != null);
            return values.ToDictionary(v => ToCamelCase(v.Name), v => ToCamelCase(v.Value.ToString()));
        }

        protected string GetConcatenatedParameters()
        {
            var parameters = GetParameters();
            if (!parameters.Any())
                return "";

            var @params = parameters.Select(p => $"{p.Key}={p.Value}");
            var paramsStr = string.Join("&", @params);
            return $"?{paramsStr}";
        }

        protected virtual T DeserializeData(JToken content)
        {
            return content.ToObject<T>();
        }

        private object GetParameterValue(PropertyInfo info)
        {
            var value = info.GetValue(this);
            if (value is Enum e)
                value = e.GetParameter();
            return value;
        }

        private static string ToCamelCase(string s)
        {
            return Char.ToLowerInvariant(s[0]) + s.Substring(1);
        }
    }
}
