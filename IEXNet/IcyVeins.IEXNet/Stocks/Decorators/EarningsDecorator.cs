﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class EarningsDecorator : EndPointDecorator<Earnings>
    {
        internal override string EndPoint => "earnings";
    }
}
