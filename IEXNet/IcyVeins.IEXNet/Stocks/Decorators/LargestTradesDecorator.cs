﻿namespace IcyVeins.IEXNet.Stocks.Decorators
{
    internal class LargestTradesDecorator : EndPointDecorator<LargestTrades>
    {
        internal override string EndPoint => "largest-trades";
    }
}
