﻿using IcyVeins.IEXNet.Stocks.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IcyVeins.IEXNet.Stocks
{
    internal sealed class StockQueryUriBuilder
    {
        private const string StockUri = IEX.BaseUri + "/stock/";

        internal static string CreateUri(List<string> symbols, List<EndPointDecorator> decorators)
        {
            var decoration = GetDecoration(symbols, decorators);
            return StockUri + decoration;
        }

        private static string GetDecoration(List<string> symbols, List<EndPointDecorator> decorators)
        {
            symbols = symbols ?? new List<string>();
            decorators = decorators ?? new List<EndPointDecorator>();

            if (symbols.Count == 1 && decorators.Count == 1)
                return GetDirectDecoration(symbols[0], decorators[0]);

            return symbols.Count == 1
                ? CreateSingleSymbolUri(symbols, decorators)
                : CreateMarketUri(symbols, decorators);
        }

        private static string GetDirectDecoration(string symbol, EndPointDecorator decorator)
        {
            var endPointDecoration = decorator.GetDirectDecoration();
            return $"{symbol}/{endPointDecoration}";
        }

        private static string CreateSingleSymbolUri(List<string> symbols, List<EndPointDecorator> decorators)
        {
            var endPoints = CreateEndPointString(symbols, decorators);
            var @params = CreateParamsString(decorators);

            var uri = $"{symbols[0]}/";
            if (decorators.Count > 1)
                uri += "batch?";
            return uri + endPoints + @params;
        }

        private static string CreateMarketUri(List<string> symbols, List<EndPointDecorator> decorators)
        {
            var symbolParams = CreateSymbolsString(symbols);
            var endPoints = CreateEndPointString(symbols, decorators);
            var @params = CreateParamsString(decorators);
            if (!string.IsNullOrWhiteSpace(symbolParams))
                endPoints = "&" + endPoints;

            return $"market/batch?{symbolParams}{endPoints}{@params}";
        }

        private static string CreateSymbolsString(List<string> symbols)
        {
            return symbols.Any() ? $"symbols={string.Join(",", symbols)}" : "";
        }

        private static string CreateEndPointString(List<string> symbols, List<EndPointDecorator> decorators)
        {
            if (!decorators.Any())
                return "";

            if (symbols.Count == 1 && decorators.Count == 1)
                return decorators[0].EndPoint;

            var endPoints = decorators.Select(d => d.EndPoint);
            return "types=" + string.Join(",", endPoints);
        }

        private static string CreateParamsString(List<EndPointDecorator> decorators)
        {
            if (!decorators.Any())
                return "";

            var paramMap = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            foreach (var decorator in decorators)
            {
                var decParams = decorator.GetParameters();
                foreach (var param in decParams)
                    paramMap[param.Key] = param.Value;
            }

            if (!paramMap.Any())
                return "";

            var parts = paramMap.Select(kv => $"&{kv.Key}={kv.Value}");
            var paramsString = string.Join("", parts);
            return Uri.EscapeUriString(paramsString);
        }
    }
}
