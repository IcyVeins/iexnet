﻿namespace IcyVeins.IEXNet.Stocks
{
    public class KeyStats
    {
        public string CompanyName { get; set; }

        /// <summary>
        /// is not calculated in real time.
        /// </summary>
        public double? Marketcap { get; set; }	
        public double? Beta { get; set; }
        public double? Week52high { get; set; }
        public double? Week52low { get; set; }
        public double? Week52change { get; set; }
        public double? ShortInterest { get; set; }
        public string ShortDate { get; set; }
        public double? DividendRate { get; set; }
        public double? DividendYield { get; set; }
        public string ExDividendDate { get; set; }
        /// <summary>
        /// (Most recent quarter)
        /// </summary>
        public double? LatestEPS { get; set; }
        public string LatestEPSDate { get; set; }
        public double? SharesOutstanding { get; set; }
        public double? Float { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? ReturnOnEquity { get; set; }
        /// <summary>
        /// (Most recent quarter)
        /// </summary>
        public double? ConsensusEPS { get; set; }
        /// <summary>
        /// (Most recent quarter)
        /// </summary>
        public double? NumberOfEstimates { get; set; }
        public string Symbol { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? EBITDA { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? Revenue { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? GrossProfit { get; set; }
        /// <summary>
        /// Refers to total cash. (Trailing twelve months)
        /// </summary>
        public double? Cash { get; set; }
        /// <summary>
        /// Refers to total debt. (Trailing twelve months)
        /// </summary>
        public double? Debt { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? TtmEPS { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? RevenuePerShare { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? RevenuePerEmployee { get; set; }
        public double? PeRatioHigh { get; set; }
        public double? PeRatioLow { get; set; }
        /// <summary>
        /// Refers to the difference between actual EPS and consensus EPS in dollars.
        /// </summary>
        public double? EPSSurpriseDollar { get; set; }
        /// <summary>
        /// Refers to the percent difference between actual EPS and consensus EPS.
        /// </summary>
        public double? EPSSurprisePercent { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? ReturnOnAssets { get; set; }
        /// <summary>
        /// (Trailing twelve months)
        /// </summary>
        public double? ReturnOnCapital { get; set; }
        public double? ProfitMargin { get; set; }
        public double? PriceToSales { get; set; }
        public double? PriceToBook { get; set; }
        public double? Day200MovingAvg { get; set; }
        public double? Day50MovingAvg { get; set; }
        /// <summary>
        /// Represents top 15 institutions
        /// </summary>
        public double? InstitutionPercent { get; set; }
        public double? InsiderPercent { get; set; }
        public double? ShortRatio { get; set; }
        public double? Year5ChangePercent { get; set; }
        public double? Year2ChangePercent { get; set; }
        public double? Year1ChangePercent { get; set; }
        public double? YtdChangePercent { get; set; }
        public double? Month6ChangePercent { get; set; }
        public double? Month3ChangePercent { get; set; }
        public double? Month1ChangePercent { get; set; }
        public double? Day5ChangePercent { get; set; }
    }
}
