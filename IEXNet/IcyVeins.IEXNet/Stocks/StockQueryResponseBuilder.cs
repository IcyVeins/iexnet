﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IcyVeins.IEXNet.Stocks
{
    public sealed class StockQueryBuilderFactory
    {
        private const int MaxSymbols = 100;

        internal StockQueryBuilderFactory() { }

        /// <summary>
        /// Creates a stock query builder for given symbols
        /// </summary>
        /// <param name="symbols">IEX symbols</param>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder For(IEnumerable<string> symbols)
        {
            if (symbols == null)
                throw new ArgumentNullException($"Argument {symbols} must not be null.", nameof(symbols));

            var querySymbols = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            
            foreach (var symbol in symbols)
            {
                if (string.IsNullOrWhiteSpace(symbol))
                    throw new ArgumentException($"Argument {symbol} must not be null, empty or consist only of white-space characters.", nameof(symbol));

                querySymbols.Add(symbol);

                if (querySymbols.Count >= MaxSymbols)
                    throw new ArgumentException($"A single request must not contain more than {MaxSymbols} symbols.", nameof(symbol));
            }

            return new StockQueryBuilder(querySymbols);
        }

        /// <summary>
        /// Creates a stock query builder for given symbols
        /// </summary>
        /// <param name="symbols">IEX symbols</param>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder For(params string[] symbols)
        {
            return For(symbols.AsEnumerable());
        }
    }
}
