﻿using System.Collections.Generic;

namespace IcyVeins.IEXNet.Stocks
{
    public class Dividends : List<Dividend>
    {
    }

    public class Dividend
    {
        /// <summary>
        /// Refers to the dividend ex-date
        /// </summary>
        public string ExDate { get; set; }

        /// <summary>
        /// Refers to the payment date
        /// </summary>
        public string PaymentDate { get; set; }

        /// <summary>
        /// Refers to the dividend record date
        /// </summary>
        public string RecordDate { get; set; }

        /// <summary>
        /// Refers to the dividend declaration date
        /// </summary>
        public string DeclaredDate { get; set; }

        /// <summary>
        /// Refers to the payment amount
        /// </summary>
        public double? Amount { get; set; }

        /// <summary>
        /// refers to the dividend flag.
        /// FI = Final dividend, div ends or instrument ends,
        /// LI = Liquidation, instrument liquidates,
        /// PR = Proceeds of a sale of rights or shares,
        /// RE = Redemption of rights,
        /// AC = Accrued dividend,
        /// AR = Payment in arrears,
        /// AD = Additional payment,
        /// EX = Extra payment,
        /// SP = Special dividend,
        /// YE = Year end,
        /// UR = Unknown rate,
        /// SU = Regular dividend is suspended
        /// </summary>
        public string Flag { get; set; }

        /// <summary>
        /// refers to the dividend payment type(Dividend income, Interest income, Stock dividend, Short term capital gain, Medium term capital gain, Long term capital gain, Unspecified term capital gain)
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// refers to the dividend income type
        /// P = Partially qualified income
        /// Q = Qualified income
        /// N = Unqualified income 
        /// null = N/A or unknown
        /// </summary>
        public string Qualified { get; set; }

        /// <summary>
        /// refers to the indicated rate of the dividend
        /// </summary>
        public float? Indicated { get; set; }
    }
}
