﻿using IcyVeins.IEXNet.Stocks.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcyVeins.IEXNet.Stocks
{
    public class StockQueryBuilder
    {
        private const int MaxSymbols = 100;
        private const int MaxEndPoints = 10;

        private List<string> _symbols;
        private Dictionary<string, EndPointDecorator> _decoratorMap = new Dictionary<string, EndPointDecorator>(StringComparer.InvariantCultureIgnoreCase);

        internal StockQueryBuilder(IEnumerable<string> symbols)
        {
            _symbols = symbols.ToList();
        }

        /// <summary>
        /// Includes chart data in the request
        /// </summary>
        /// <param name="chartSimplify">If true, runs a polyline simplification using the Douglas-Peucker algorithm. This is useful if plotting sparkline charts.</param>
        /// <param name="changeFromClose">If true, changeOverTime and marketChangeOverTime will be relative to previous day close instead of the first value.</param>
        /// <param name="chartInterval">If passed, chart data will return every Nth element as defined by chartInterval</param>
        /// <param name="chartLast">If passed, chart data will return the last N elements</param>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Chart(bool? chartSimplify = default(bool?), bool? chartInterval = default(bool?),
            bool? changeFromClose = default(bool?), int? chartLast = default(int?))
        {
            return AddDecorator(new ChartDecorator(default(bool?), chartSimplify, chartInterval, changeFromClose, chartLast));
        }

        /// <summary>
        /// Includes chart data in the request
        /// </summary>
        /// <param name="range">Period range to query</param>
        /// <param name="chartReset">If true, 1d chart will reset at midnight instead of the default behavior of 9:30am ET.</param>
        /// <param name="chartSimplify">If true, runs a polyline simplification using the Douglas-Peucker algorithm. This is useful if plotting sparkline charts.</param>
        /// <param name="changeFromClose">If true, changeOverTime and marketChangeOverTime will be relative to previous day close instead of the first value.</param>
        /// <param name="chartInterval">If passed, chart data will return every Nth element as defined by chartInterval</param>
        /// <param name="chartLast">If passed, chart data will return the last N elements</param>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Chart(Range range, bool? chartReset = default(bool?), bool? chartSimplify = default(bool?),
            bool? chartInterval = default(bool?), bool? changeFromClose = default(bool?), int? chartLast = default(int?))
        {
            return AddDecorator(new ChartDecorator(range, chartReset, chartSimplify, chartInterval, changeFromClose, chartLast));
        }

        /// <summary>
        /// Includes chart data in the request
        /// </summary>
        /// <param name="date">IEX-only data by minute for a specified date.</param>
        /// <param name="chartSimplify">If true, runs a polyline simplification using the Douglas-Peucker algorithm. This is useful if plotting sparkline charts.</param>
        /// <param name="changeFromClose">If true, changeOverTime and marketChangeOverTime will be relative to previous day close instead of the first value.</param>
        /// <param name="chartInterval">If passed, chart data will return every Nth element as defined by chartInterval</param>
        /// <param name="chartLast">If passed, chart data will return the last N elements</param>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Chart(DateTime date, bool? chartSimplify = default(bool?), bool? chartInterval = default(bool?),
            bool? changeFromClose = default(bool?), int? chartLast = default(int?))
        {
            return AddDecorator(new ChartDecorator(date, default(bool?), chartSimplify, chartInterval, changeFromClose, chartLast));
        }

        /// <summary>
        /// Includes chart data in the request
        /// </summary>
        /// <param name="dynamic">Will return 1d or 1m data depending on the day or week and time of day. Intraday per minute data is only returned during market hours.</param>
        /// <param name="chartSimplify">If true, runs a polyline simplification using the Douglas-Peucker algorithm. This is useful if plotting sparkline charts.</param>
        /// <param name="changeFromClose">If true, changeOverTime and marketChangeOverTime will be relative to previous day close instead of the first value.</param>
        /// <param name="chartInterval">If passed, chart data will return every Nth element as defined by chartInterval</param>
        /// <param name="chartLast">If passed, chart data will return the last N elements</param>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Chart(bool dynamic, bool? chartSimplify = default(bool?), bool? chartInterval = default(bool?),
            bool? changeFromClose = default(bool?), int? chartLast = default(int?))
        {
            return AddDecorator(new ChartDecorator(dynamic, default(bool?), chartSimplify, chartInterval, changeFromClose, chartLast));
        }

        /// <summary>
        /// Includes company data in the request
        /// </summary>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Company()
        {
            return AddDecorator(new CompanyDecorator());
        }

        /// <summary>
        /// Includes dividends in the request
        /// </summary>
        /// <param name="range">Period range to query</param>
        /// <returns></returns>
        public StockQueryBuilder Dividends(Range? range = default(Range?))
        {
            return AddDecorator(new DividendsDecorator(range));
        }

        /// <summary>
        /// Includes delayed quotes data in the request
        /// </summary>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder DelayedQuote()
        {
            return AddDecorator(new DelayedQuoteDecorator());
        }

        /// <summary>
        /// Includes earnings data in the request
        /// </summary>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Earnings()
        {
            return AddDecorator(new EarningsDecorator());
        }

        /// <summary>
        /// Includes effective-spread data in the request
        /// </summary>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder EffectiveSpread()
        {
            return AddDecorator(new EffectiveSpreadDecorator());
        }

        /// <summary>
        /// Includes financials data in the request
        /// </summary>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Financials()
        {
            return AddDecorator(new FinancialsDecorator());
        }

        /// <summary>
        /// Includes key stats in the request
        /// </summary>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder KeyStats()
        {
            return AddDecorator(new KeyStatsDecorator());
        }

        /// <summary>
        /// Includes largest-trades data in the request
        /// </summary>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder LargestTrades()
        {
            return AddDecorator(new LargestTradesDecorator());
        }

        /// <summary>
        /// Includes peers data in the request
        /// </summary>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Peers()
        {
            return AddDecorator(new PeersDecorator());
        }

        /// <summary>
        /// Includes quotes data in the request
        /// </summary>
        /// <param name="displayPercent">If set to true, all percentage values will be multiplied by a factor of 100</param>
        /// <returns>StockRequestBuilder</returns>
        public StockQueryBuilder Quote(bool? displayPercent = default(bool?))
        {
            return AddDecorator(new QuoteDecorator(displayPercent));
        }

        public StockQueryResponse RequestSync()
        {
            return Request().Result;
        }

        public async Task<StockQueryResponse> Request()
        {
            if (!_decoratorMap.Any())
                return new StockQueryResponse();

            var symbols = _symbols.ToList();
            var endPoints = _decoratorMap.Values.ToList();
            var uri = StockQueryUriBuilder.CreateUri(symbols, endPoints);
            var content = await WebRequester.Get(uri).ConfigureAwait(false);
            return new StockQueryResponse(symbols, endPoints, content);
        }

        private StockQueryBuilder AddDecorator(EndPointDecorator decorator)
        {
            if (_decoratorMap.Count >= MaxEndPoints)
                throw new ArgumentException($"A single request must not contain more than {MaxEndPoints} data end points.");

            var endPoint = decorator.EndPoint;
            _decoratorMap[endPoint] = decorator;
            return this;
        }
    }
}
