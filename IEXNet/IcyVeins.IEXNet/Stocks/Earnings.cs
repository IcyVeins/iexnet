﻿using Newtonsoft.Json;

namespace IcyVeins.IEXNet.Stocks
{
    public class Earnings
    {
        public string Symbol { get; set; }
        [JsonProperty("earnings")] public EarningsData[] EarningsDatas { get; set; }
    }

    public class EarningsData
    {
        /// <summary>
        /// Actual earnings per share for the period.
        /// </summary>
        public double? ActualEPS { get; set; }

        /// <summary>
        /// Consensus EPS estimate trend for the period.
        /// </summary>
        public double? ConsensusEPS { get; set; }

        /// <summary>
        /// Earnings per share estimate for the period.
        /// </summary>
        public double? EstimatedEPS { get; set; }

        /// <summary>
        /// Time of earnings announcement.BTO(Before open), DMT(During trading), AMC(After close)
        /// </summary>
        public string AnnounceTime { get; set; }

        /// <summary>
        /// Number of estimates for the period
        /// </summary>
        public double? NumberOfEstimates { get; set; }

        /// <summary>
        /// Dollar amount of EPS surprise for the period
        /// </summary>
        public double? EPSSurpriseDollar { get; set; }

        /// <summary>
        /// Expected earnings report date YYYY-MM-DD
        /// </summary>
        public string EPSReportDate { get; set; }

        /// <summary>
        /// The fiscal quarter the earnings data applies to Q# YYYY
        /// </summary>
        public string FiscalPeriod { get; set; }

        /// <summary>
        /// Date representing the company fiscal quarter end YYYY-MM-DD
        /// </summary>
        public string FiscalEndDate { get; set; }

        /// <summary>
        /// Represents the EPS of the quarter a year ago
        /// </summary>
        public double? YearAgo { get; set; }

        /// <summary>
        /// Represents the percent difference between the quarter a year ago actualEPS and current period actualEPS.
        /// </summary>
        public double? YearAgoChangePercent { get; set; }

        /// <summary>
        /// Represents the percent difference between the quarter a year ago actualEPS and current period estimatedEPS.
        /// </summary>
        public double? EstimatedChangePercent { get; set; }

        /// <summary>
        /// Represents the IEX id for the stock
        /// </summary>
        public double? SymbolId { get; set; }
    }
}
