﻿using System.Collections.Generic;

namespace IcyVeins.IEXNet.Stocks
{
    public class Chart : List<ChartData>
    {
    }

    public class ChartData
    {
        /// <summary>
        /// Is only available on 1d chart.
        /// </summary>
        public string Minute { get; set; }

        /// <summary>
        /// Is only available on 1d chart. 15 minute delayed
        /// </summary>
        public double? MarketAverage { get; set; }

        /// <summary>
        /// Is only available on 1d chart. 15 minute delayed
        /// </summary>
        public double? MarketNotional { get; set; }

        /// <summary>
        /// Is only available on 1d chart. 15 minute delayed
        /// </summary>
        public double? MarketNumberOfTrades { get; set; }

        /// <summary>
        /// Is only available on 1d chart. 15 minute delayed
        /// </summary>
        public double? MarketOpen { get; set; }

        /// <summary>
        /// Is only available on 1d chart. 15 minute delayed
        /// </summary>
        public double? MarketClose { get; set; }

        /// <summary>
        /// Is only available on 1d chart. 15 minute delayed
        /// </summary>
        public double? MarketHigh { get; set; }

        /// <summary>
        /// Is only available on 1d chart. 15 minute delayed
        /// </summary>
        public double? MarketLow { get; set; }

        /// <summary>
        /// Is only available on 1d chart. 15 minute delayed
        /// </summary>
        public double? MarketVolume { get; set; }

        /// <summary>
        /// Is only available on 1d chart.Percent change of each interval relative to first value. 15 minute delayed
        /// </summary>
        public double? MarketChangeOverTime { get; set; }

        /// <summary>
        /// Is only available on 1d chart.
        /// </summary>
        public double? Average { get; set; }

        /// <summary>
        /// Is only available on 1d chart.
        /// </summary>
        public double? Notional { get; set; }

        /// <summary>
        /// Is only available on 1d chart.
        /// </summary>
        public double? NumberOfTrades { get; set; }

        /// <summary>
        /// Is only available on 1d chart, and only when chartSimplify is true.
        /// The first element is the original number of points.
        /// Second element is how many remain after simplification.
        /// </summary>
        public int[] SimplifyFactor { get; set; }

        /// <summary>
        /// Iis available on all charts.
        /// </summary>
        public double? High { get; set; }

        /// <summary>
        /// Is available on all charts.
        /// </summary>
        public double? Low { get; set; }

        /// <summary>
        /// Is available on all charts.
        /// </summary>
        public double? Volume { get; set; }

        /// <summary>
        /// Is available on all charts. A variable formatted version of the date depending on the range.
        /// Optional convienience field.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Is available on all charts. Percent change of each interval relative to first value.
        /// Useful for comparing multiple stocks.
        /// </summary>
        public double? ChangeOverTime { get; set; }

        /// <summary>
        /// Is available on all charts.
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Is available on all charts.
        /// </summary>
        public double? Open { get; set; }

        /// <summary>
        /// Is available on all charts.
        /// </summary>
        public double? Close { get; set; }

        /// <summary>
        /// Is not available on 1d chart.
        /// </summary>
        public double? UnadjustedVolume { get; set; }

        /// <summary>
        /// Is not available on 1d chart.
        /// </summary>
        public double? Change { get; set; }

        /// <summary>
        /// Is not available on 1d chart.
        /// </summary>
        public double? ChangePercent { get; set; }

        /// <summary>
        /// Is not available on 1d chart.
        /// </summary>
        public double? Vwap { get; set; }
    }
}
