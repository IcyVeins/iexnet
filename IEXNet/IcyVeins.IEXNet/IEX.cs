﻿using IcyVeins.IEXNet.ReferenceData;
using IcyVeins.IEXNet.Stocks;
using System.Threading.Tasks;

namespace IcyVeins.IEXNet
{
    /// <summary>
    /// Interface provider for the complete IEX api
    /// <see cref="https://iextrading.com/developer/docs/#getting-started"/>
    /// </summary>
    public static class IEX
    {
        internal const string BaseUri = "https://api.iextrading.com/1.0";

        public static StockQueryBuilderFactory Stocks => new StockQueryBuilderFactory();

        public static SymbolData[] GetSymbolsSync()
        {
            return GetSymbols().Result;
        }

        public static Task<SymbolData[]> GetSymbols()
        {
            return SymbolFetcher.FetchSymbols();
        }
    }
}
