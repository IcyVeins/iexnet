﻿using System;

namespace IcyVeins.IEXNet
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    sealed class EnumParameter : Attribute
    {
        public readonly string Value;

        public EnumParameter(string value)
        {
            Value = value;
        }
    }
}
