﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace IcyVeins.IEXNet
{
    /// <summary>
    /// Internal utility class for handling web requests and responses
    /// </summary>
    public static class WebRequester
    {
        public static async Task<string> Get(string uri)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(uri).ConfigureAwait(false);
                if (!response.IsSuccessStatusCode)
                    return null;

                return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }

        internal static async Task<T> Get<T>(string uri)
        {
            var content = await Get(uri);
            return new JavaScriptSerializer().Deserialize<T>(content);
        }
    }
}
