﻿using System;

namespace IcyVeins.IEXNet.CommonIssueTypes
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    internal sealed class CommonIssueTypeAttribute : Attribute
    {
        internal readonly string Key;
        internal readonly string PrettyName;

        public CommonIssueTypeAttribute(string key, string prettyName)
        {
            Key = key;
            PrettyName = prettyName;
        }
    }
}
