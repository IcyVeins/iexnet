﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IcyVeins.IEXNet.CommonIssueTypes
{
    internal static class CommonIssueTypeHelper
    {
        private static readonly Lazy<Dictionary<string, CommonIssueType>> _keyMap = new Lazy<Dictionary<string, CommonIssueType>>(CreateKeyMap);
        private static readonly Lazy<Dictionary<CommonIssueType, string>> _prettyNameMap = new Lazy<Dictionary<CommonIssueType, string>>(CreatePrettyNameMap);

        internal static CommonIssueType ToCommonIssueType(string type)
        {
            return _keyMap.Value[type];
        }

        internal static string GetPrettyName(CommonIssueType issueType)
        {
            return _prettyNameMap.Value[issueType];
        }

        private static Dictionary<string, CommonIssueType> CreateKeyMap()
        {
            var values = (CommonIssueType[])Enum.GetValues(typeof(CommonIssueType));
            return values.ToDictionary(v => GetAttribute(v).Key);
        }

        private static Dictionary<CommonIssueType, string> CreatePrettyNameMap()
        {
            var values = (CommonIssueType[])Enum.GetValues(typeof(CommonIssueType));
            return values.ToDictionary(v => v, v => GetAttribute(v).Key);
        }

        private static CommonIssueTypeAttribute GetAttribute(CommonIssueType issueType)
        {
            var type = typeof(CommonIssueType);
            var memInfo = type.GetMember(issueType.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(CommonIssueTypeAttribute), false);
            return ((CommonIssueTypeAttribute)attributes[0]);
        }
    }
}
