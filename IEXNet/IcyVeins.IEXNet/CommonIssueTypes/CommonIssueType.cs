﻿namespace IcyVeins.IEXNet.CommonIssueTypes
{
    public enum CommonIssueType
    {
        [CommonIssueType("", "Not Available")]
        NotAvailable,

        [CommonIssueType("AD", "American Depositary Receipt")]
        AmericanDepositaryReceipt,

        [CommonIssueType("CE", "Closed End Fund")]
        ClosedEndFund,

        [CommonIssueType("CS", "Common Stock")]
        CommonStock,

        [CommonIssueType("ET", "Exchange-Traded Fund")]
        ETF,

        [CommonIssueType("LP", "Limited Partnerships")]
        LimitedPartnerships,

        [CommonIssueType("RE", "Real Estate Investment Trust")]
        RealEstateInvestmentTrust,

        [CommonIssueType("SI", "Secondary Issue")]
        SecondaryIssue,
    }
}
