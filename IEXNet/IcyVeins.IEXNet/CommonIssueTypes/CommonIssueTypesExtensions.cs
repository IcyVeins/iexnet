﻿namespace IcyVeins.IEXNet.CommonIssueTypes
{
    public static class CommonIssueTypesExtensions
    {
        public static string GetPrettyName(this CommonIssueType issueType)
        {
            return CommonIssueTypeHelper.GetPrettyName(issueType);
        }
    }
}
