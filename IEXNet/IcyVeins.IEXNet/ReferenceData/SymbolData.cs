﻿using IcyVeins.IEXNet.CommonIssueTypes;
using System;

namespace IcyVeins.IEXNet.ReferenceData
{
    public class SymbolData
    {
        /// <summary>
        /// Refers to the symbol represented in Nasdq Integrated symbology (INET).
        /// <see cref="http://www.nasdaqtrader.com/trader.aspx?id=CQSsymbolconvention"/>
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// Refers to the name of the company or security.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Refers to the date the symbol reference data was generated.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Will be true if the symbol is enabled for trading on IEX.
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Refers to the common issue type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Unique ID applied by IEX to track securities through symbol changes
        /// </summary>
        public string IEXId { get; set; }

        public CommonIssueType GetCommonIssueType()
        {
            return CommonIssueTypeHelper.ToCommonIssueType(Type);
        }
    }
}
