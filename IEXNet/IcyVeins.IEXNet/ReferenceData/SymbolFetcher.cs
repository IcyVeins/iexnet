﻿using System.Threading.Tasks;

namespace IcyVeins.IEXNet.ReferenceData
{
    internal static class SymbolFetcher
    {
        private const string Symbols = "/ref-data/symbols";

        private const string SymbolUri = IEX.BaseUri + Symbols;

        internal static async Task<SymbolData[]> FetchSymbols()
        {
            return await WebRequester.Get<SymbolData[]>(SymbolUri);
        }
    }
}
