﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace IcyVeins.IEXNet.Tests
{
    [TestClass]
    public class ReferenceDataIntegrationTests
    {
        [TestMethod]
        public void FetchSymbols()
        {
            var symbols = IEX.GetSymbolsSync();
            Assert.IsNotNull(symbols);
            Assert.IsTrue(symbols.Any());
        }
    }
}
