﻿using IcyVeins.IEXNet.Stocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IcyVeins.IEXNet.Tests
{
    [TestClass]
    public class StocksIntegrationTests
    {
        [TestMethod]
        public void SingleSymbol_SingleEndPoint()
        {
            var response = IEX.Stocks.For("aapl").Company().RequestSync();
            Assert.IsNull(response.Get<Company>("fb"));
            Assert.IsNotNull(response.Get<Company>("aapl"));
        }

        [TestMethod]
        public void MultiSymbol_SingleEndPoint()
        {
            var response = IEX.Stocks.For("aapl", "fb").Company().RequestSync();
            Assert.IsNull(response.Get<Company>("htht"));
            Assert.IsNotNull(response.Get<Company>("fb"));
            Assert.IsNotNull(response.Get<Company>("aapl"));
        }

        [TestMethod]
        public void SingleSymbol_MultiEndPoint()
        {
            var response = IEX.Stocks.For("aapl").Company().DelayedQuote().RequestSync();
            Assert.IsNull(response.Get<Company>("fb"));
            Assert.IsNull(response.Get<DelayedQuote>("fb"));
            Assert.IsNotNull(response.Get<Company>("aapl"));
            Assert.IsNotNull(response.Get<DelayedQuote>("aapl"));
        }

        [TestMethod]
        public void MultiSymbol_MultiEndPoint()
        {
            var response = IEX.Stocks.For("aapl", "fb").Company().DelayedQuote().RequestSync();
            Assert.IsNull(response.Get<Company>("htht"));
            Assert.IsNull(response.Get<DelayedQuote>("htht"));
            Assert.IsNotNull(response.Get<Company>("aapl"));
            Assert.IsNotNull(response.Get<DelayedQuote>("aapl"));
            Assert.IsNotNull(response.Get<Company>("fb"));
            Assert.IsNotNull(response.Get<DelayedQuote>("fb"));
        }

        [TestMethod]
        public void Quote()
        {
            var noPercent = IEX.Stocks.For("aapl").Quote(false).RequestSync().Get<Quote>("aapl");
            var percent = IEX.Stocks.For("aapl").Quote(true).RequestSync().Get<Quote>("aapl");
            Assert.IsNotNull(noPercent);
            Assert.IsNotNull(percent);
        }

        [TestMethod]
        public void Chart()
        {
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart().RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(false).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(true).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.OneDay).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.OneMonth).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.ThreeMonths).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.SixMonths).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.OneYear).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.TwoYears).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.FiveYears).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.YearToDate).RequestSync().Get<Chart>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Chart(Range.OneDay, chartReset: true).RequestSync().Get<Chart>("aapl"));
        }

        [TestMethod]
        public void Dividends()
        {
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends().RequestSync().Get<Dividends>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends(Range.FiveYears).RequestSync().Get<Dividends>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends(Range.OneDay).RequestSync().Get<Dividends>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends(Range.OneMonth).RequestSync().Get<Dividends>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends(Range.OneYear).RequestSync().Get<Dividends>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends(Range.SixMonths).RequestSync().Get<Dividends>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends(Range.ThreeMonths).RequestSync().Get<Dividends>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends(Range.TwoYears).RequestSync().Get<Dividends>("aapl"));
            Assert.IsNotNull(IEX.Stocks.For("aapl").Dividends(Range.YearToDate).RequestSync().Get<Dividends>("aapl"));
        }

        [TestMethod]
        public void Earnings()
        {
            var response = IEX.Stocks.For("aapl").Earnings().RequestSync();
            Assert.IsNotNull(response.GetEarnings("aapl"));
        }

        [TestMethod]
        public void EffectiveSpread()
        {
            var response = IEX.Stocks.For("aapl").EffectiveSpread().RequestSync();
            Assert.IsNotNull(response.GetEffectiveSpread("aapl"));
        }

        [TestMethod]
        public void Financials()
        {
            Assert.IsNotNull(IEX.Stocks.For("aapl").Financials().RequestSync().GetFinancials("aapl"));
        }

        [TestMethod]
        public void KeyStats()
        {
            Assert.IsNotNull(IEX.Stocks.For("aapl").KeyStats().RequestSync().GetKeyStats("aapl"));
        }

        [TestMethod]
        public void LargestTrades()
        {
            Assert.IsNotNull(IEX.Stocks.For("aapl").LargestTrades().RequestSync().GetLargestTrades("aapl"));
        }

        [TestMethod]
        public void ChartAndDividends()
        {
            var response = IEX.Stocks.For("aapl", "fb").Dividends(Range.OneYear).Chart(Range.OneYear).RequestSync();
            Assert.IsNotNull(response.Get<Chart>("aapl"));
            Assert.IsNotNull(response.Get<Dividends>("aapl"));
        }

        [TestMethod]
        public void Peers()
        {
            var single = IEX.Stocks.For("aapl").Peers().RequestSync();
            var multi = IEX.Stocks.For("aapl", "fb").Peers().RequestSync();
            Assert.IsNotNull(single.GetPeers("aapl"));
            Assert.IsNotNull(multi.GetPeers("aapl"));
            Assert.IsNotNull(multi.GetPeers("fb"));
        }
    }
}
